#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from OnePlus7T device
$(call inherit-product, device/oneplus/OnePlus7T/device.mk)

PRODUCT_DEVICE := OnePlus7T
PRODUCT_NAME := lineage_OnePlus7T
PRODUCT_BRAND := OnePlus
PRODUCT_MODEL := HD1901
PRODUCT_MANUFACTURER := oneplus

PRODUCT_GMS_CLIENTID_BASE := android-oneplus

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="OnePlus7T-user 12 SKQ1.211113.001 Q.202212132005 release-keys"

BUILD_FINGERPRINT := OnePlus/OnePlus7T/OnePlus7T:12/SKQ1.211113.001/Q.202212132005:user/release-keys
